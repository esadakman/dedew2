$(".navbar .nav-link").on("click", function () {
  $(".navbar").find(".active").removeClass("active");
  $(this).addClass("active");
});

$("#myModal").on("shown.bs.modal", function () {
  $("#myInput").trigger("focus");
});

//  ! Products Blur
$(".box").hover(
  function () {
    $(".grid__container__right").addClass("blur");
  },
  function () {
    $(".grid__container__right").removeClass("blur");
  }
);
$(".grid__container__right").hover(
  function () {
    $(".box").addClass("blur");
  },
  function () {
    $(".box").removeClass("blur");
  }
);
 
